import java.util.*;



public class MonkAndChamberOfSecrets {

    
    public static void main(String[] args) {

        Scanner scannerInput = new Scanner(System.in);
        int noOfSpider = scannerInput.nextInt();
        int selectedSpider = scannerInput.nextInt();
        Queue<Spider> queue = new LinkedList<>();
        for (int i = 0; i < noOfSpider; i++){
            Spider spiderObject = new Spider();
            spiderObject.spiderPower = scannerInput.nextInt();
            spiderObject.index = i+1;
            queue.add(spiderObject);
        }

        Spider[] temporaryArray = new Spider[selectedSpider];

        for (int i = 0; i < selectedSpider; i++){
            int j = 0, maxIndex = 0;
            while (!queue.isEmpty() && j < selectedSpider){
                Spider tempSpider = queue.remove();
                temporaryArray[j] = tempSpider;
                if (tempSpider.spiderPower > temporaryArray[maxIndex].spiderPower){
                    maxIndex = j;
                }
                j++;
            }
            System.out.print(temporaryArray[maxIndex].index + " ");
            j--;
            for (int k = 0 ; k <= j; k++){
                if (temporaryArray[k].spiderPower > 0){
                    temporaryArray[k].spiderPower -= 1;
                }
                if (k != maxIndex){
                    queue.add(temporaryArray[k]);
                }
            }
        }
    }
}
 class Spider {
        int spiderPower;
        int index;
    }
