package aa;

import java.util.*;

public class MonkAndChamberOfSecret {

    Spider maxIndex;
    Queue < Spider > queue;
    Scanner scannerInput = new Scanner(System.in);

    public String[] acceptingInput() {

        System.out.println("Enter no of spider");
        String spiderInput = scannerInput.nextLine();
        String[] spiderArray = spiderInput.split(" ");
        return spiderArray;
    }

    public Queue < Spider > addingSpiderToQueue(int noOfSpider) {
        Queue < Spider > queue = new LinkedList();
        for (int i = 0; i < noOfSpider; i++) {
            int spiderPower = scannerInput.nextInt();
            Spider spiderObject = new Spider(spiderPower, i + 1);
            queue.add(spiderObject);

        }
        return queue;
    }

    public Spider[] selectSpiderBatch(int selectedSpider, Queue queue, Spider[] temporaryArray) {
int j=0;
        while (!queue.isEmpty() && j < selectedSpider) {

            Spider tempSpider = (Spider) queue.remove();

            temporaryArray[j] = tempSpider;

            j++;
        }
        return temporaryArray;
    }

    public Spider findMaxPower(Spider[] temporaryArray) {

        maxIndex = temporaryArray[0];

        for (int i = 0; i < temporaryArray.length; i++) {

            if (temporaryArray[i].spiderPower > maxIndex.spiderPower) {

                // System.out.println(maxIndex.spiderPower);
                maxIndex = temporaryArray[i];
            }
        }
        return maxIndex;
    }

    public void displayingMaximumPowerIndex(Spider maximumPower) {
        System.out.print(maximumPower.spiderOriginalIndex + " ");
    }

    public Spider[] reducingPower(Spider[] temporaryArray) {
        for (int i = 0; i < temporaryArray.length; i++) {
            if (temporaryArray[i].spiderPower > 0) {
                temporaryArray[i].spiderPower -= 1;

            }
        }
        return temporaryArray;
    }

    public void addingReducedPowerToList(Spider[] temporaryArray, Queue queue, int spiderToBeExclude) {
        for (int i = 0; i < temporaryArray.length; i++) {
            if (temporaryArray[i].spiderPower != spiderToBeExclude) {
                queue.add(temporaryArray[i]);
            }
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        MonkAndChamberOfSecret object = new MonkAndChamberOfSecret();
        String[] spiderArray = object.acceptingInput();
        int noOfSpider = Integer.parseInt(spiderArray[0]);
        int selectedSpider = Integer.parseInt(spiderArray[1]);

        Queue < Spider > queue = object.addingSpiderToQueue(noOfSpider);
        Spider[] temporaryArray = new Spider[selectedSpider];

        for (int i = 0; i < selectedSpider; i++) {
           
            Spider[] subArray = object.selectSpiderBatch(selectedSpider, queue, temporaryArray);
            Spider maxPower = object.findMaxPower(temporaryArray);

            object.displayingMaximumPowerIndex(maxPower);
            subArray = object.reducingPower(subArray);
            object.addingReducedPowerToList(subArray, queue, (maxPower.spiderPower));

        }

    }
}